FROM debian:bullseye-slim
MAINTAINER antoine@barbare.me

RUN apt-get update -y && apt-get install -y php7.4 php7.4-fpm php7.4-gd php7.4-xml nginx supervisor curl tar

RUN mkdir -p /run/nginx && \
    mkdir -p /var/www

ADD nginx.conf /etc/nginx/nginx.conf
ADD supervisord.conf /etc/supervisord.conf
ADD start.sh /start.sh

RUN echo "cgi.fix_pathinfo = 0;" >> /etc/php/7.4/fpm/php.ini && \
    sed -i -e "s|;daemonize\s*=\s*yes|daemonize = no|g" /etc/php/7.4/fpm/php-fpm.conf && \
    sed -i -e "s|listen\s*=\s*127\.0\.0\.1:9000|listen = /var/run/php-fpm7.sock|g" /etc/php/7.4/fpm/pool.d/www.conf && \
    sed -i -e "s|;listen\.owner\s*=\s*|listen.owner = |g" /etc/php/7.4/fpm/pool.d/www.conf && \
    sed -i -e "s|;listen\.group\s*=\s*|listen.group = |g" /etc/php/7.4/fpm/pool.d/www.conf && \
    sed -i -e "s|;listen\.mode\s*=\s*|listen.mode = |g" /etc/php/7.4/fpm/pool.d/www.conf && \
    chmod +x /start.sh

EXPOSE 80

CMD /start.sh
