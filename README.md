# pica-nginx

Ce dossier contient une image nginx + PHP FPM maintenue par l'association.

Elle sert de base à tous les sites web simples hébergés par Picasoft (ex: `www`, `school`, `radio`...) et permet d'utiliser PHP sans configuration supplémentaire.

Le fichier `docker-compose.yml` associé permet de lancer tous les conteneurs des sites, sans contenu (il faudra le copier ultérieurement, avec un `docker cp` dans `/var/www/html` par exemple).

À noter que l'utilisation de `supervisord` est sans doute un peu datée, même si elle fonctionne, et que l'injection de la configuration pour `autoindex` dans [start.sh](./start.sh) est un peu bourrine, il y a sans doute une amélioration de ce côté.

## Lancement

Si tous les sites sont lancés sur la même machine, un simple `docker-compose up -d` suffira.

Autrement, il faudra préciser à `docker-compose` le nom des services à lancer.

## Mise à jour de l'image

Mettre à jour le Dockerfile si besoin, puis changer la clé sous `x-image-name: &NGINX_IMAGE` dans le fichier `docker-compose.yml` en indiquant la date du jour.

Par exemple, `registry.picasoft.net/pica-nginx:stretch-20200323` deviendra `registry.picasoft.net/pica-nginx:stretch-20200714` si je mets à jour l'image le 14 juillet 2020.

Relancer les sites sur la machine de production.

## Ajouter un nouveau site

Voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminserv:sites:website).
